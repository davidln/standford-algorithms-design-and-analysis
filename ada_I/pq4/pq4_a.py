#!/usr/bin/python

import sys
import resource

sys.setrecursionlimit(100000)
resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY,resource.RLIM_INFINITY))

#d = (0,1)
d = (1,0)
ady_list = dict()
nodes = dict()
max_node = 0
fh = open(sys.argv[1])
for line in fh :
    line = line.split()
    line = map(int, line)
    max_node = max([max_node, line[0], line[1]])
    nodes[line[0]] = 1
    nodes[line[1]] = 1
    if line[d[0]] == line[d[1]] :
        continue

    if line[d[0]] in ady_list :
        ady_list[line[d[0]]].append(line[d[1]])
    else :
        ady_list[line[d[0]]] = [line[d[1]]]

fh.close()

visited_nodes = dict()

def dfs(nodes, edges, s) :
    nodes[s] = 1
    if s in edges :
        for v in edges[s]:
            if v not in nodes :
                dfs(nodes, edges, v)
    print s

for i in range(max_node, 0, -1):
    if i not in visited_nodes :
        dfs(visited_nodes, ady_list, i)

