#!/usr/bin/python

import sys
import resource

sys.setrecursionlimit(100000)
resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY,resource.RLIM_INFINITY))

def add_edge(g, head, tail) :
    if head in g :
        g[head].append(tail)
    else :
        g[head] = [tail]

def dfs_s1(ft, nodes, edges, s) :
    nodes[s] = 1
    if s in edges :
        for v in edges[s]:
            if v not in nodes :
                dfs_s1(ft, nodes, edges, v)
    ft.append(s)

def dfs_s2(nodes, edges, s) :
    nodes[s] = 1
    n = 1
    if s in edges :
        for v in edges[s] :
            if v not in nodes :
                n += dfs_s2(nodes, edges, v)
    return n

G  = dict() # direct graph
Gr = dict() # reverse graph
max_node = 0

if len(sys.argv) > 1 :
    input_file = sys.argv[1]
else :
    input_file = "SCC.txt"

# reading graph edges and building the adyacency lists
fh = open(input_file)
for line in fh :
    line = line.split()
    line = map(int, line)
    head = line[0]
    tail = line[1]
    max_node = max([max_node, head, tail])

    if head == tail :
        continue

    add_edge(G,  head, tail)
    add_edge(Gr, tail, head)

fh.close()

visited_nodes = dict()
nodes_by_ft = list() # nodes ordered by finishing times


for i in range(max_node, 0, -1):
    if i not in visited_nodes :
        dfs_s1(nodes_by_ft, visited_nodes, Gr, i)

nodes_by_ft.reverse()
visited_nodes = dict()

scc_sizes = list()
for i in nodes_by_ft :
    if i not in visited_nodes :
        scc_sizes.append(dfs_s2(visited_nodes, G, i))

scc_sizes.sort()
scc_sizes.reverse()
print scc_sizes[:5]
