#!/usr/bin/python

import sys
import resource

sys.setrecursionlimit(100000)
resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY,resource.RLIM_INFINITY))

d = (0,1)
#d = (1,0)
ady_list = dict()
fh = open(sys.argv[1])
for line in fh :
    line = line.split()
    line = map(int, line)
    if line[d[0]] == line[d[1]] :
        continue

    if line[d[0]] in ady_list :
        ady_list[line[d[0]]].append(line[d[1]])
    else :
        ady_list[line[d[0]]] = [line[d[1]]]

fh.close()

nodes = []
fh = open(sys.argv[2])
for line in fh:
    nodes.append(int(line))

nodes.reverse()

visited_nodes = dict()

def dfs(nodes, edges, s) :
    nodes[s] = 1
    n = 1
    if s in edges :
        for v in edges[s] :
            if v not in nodes :
                n += dfs(nodes, edges, v)
    return n

for i in nodes :
    if i not in visited_nodes :
        print dfs(visited_nodes, ady_list, i)

