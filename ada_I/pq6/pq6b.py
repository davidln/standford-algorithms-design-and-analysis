#!/usr/bin/python

def ins_sort(array) :
    i = len(array) - 1
    while array[i] < array[i-1] :
        aux = array[i-1]
        array[i-1] = array[i]
        array[i] = aux
        i -= 1
        if i == 0 : break
    
fh = open("Median.txt")

array = []
means = []
c = 0
for line in fh :
    c += 1
    if c % 100 == 0: print c
    array.append(int(line))
    ins_sort(array)
    if c % 2 == 0 :
        mp = c / 2
    else :
        mp = c / 2 + 1

    means.append(array[mp-1])
fh.close()

print sum(means) % 10000
