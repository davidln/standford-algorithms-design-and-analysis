#!/usr/bin/python

import timeit

def bsearch(array, value) :
    l = 0
    u = len(array)-1
    t = (l + u) / 2
    watchdog = u
    while u-l > 1 and watchdog > 0:
        watchdog -= 1
        if array[t] == value: return t
        if array[t] > value:
            u = t
            t = (u+l)/2
        if array[t] < value:
            l = t
            t = (u+l)/2
    return l

items = dict()
fh = open("algo1_programming_prob_2sum.txt")
for line in fh :
    num = int(line)
    items[num] = 1
fh.close()

litems = items.keys()
litems.sort()

sums = dict()
c = 0
ti1 = timeit.default_timer()
for i in range(0,len(litems)) :
    c += 1
    if c % 100 == 0:
        ti2 = timeit.default_timer() - ti1
        #print "Iteracion: ", c, ti2
        ti1 = timeit.default_timer()
    v = litems[i]
    #bst1 = timeit.default_timer()
    t1 = bsearch(litems, -10000 - v)
    t2 = bsearch(litems,  10000 - v)
    #bst2 = timeit.default_timer()
    #print "Intervalo de chequeo: ", t1, t2, bst2 - bst1
    r_min = min([t1, t2])
    r_max = max([t1, t2])
    #lt1 = timeit.default_timer()
    for j in range(r_min, r_max + 1) :
        if i == j : continue
        sum_value = v + litems[j]
        if sum_value < -10000 : continue
        if sum_value >  10000 : continue
        if sum_value in sums:
            sums[sum_value].append((i, j, litems[i], litems[j]))
        else :
            sums[sum_value] = [(i, j, litems[i], litems[j])]
    #lt2 = timeit.default_timer()
    #print "Tiempo en bucle: ", lt2 -lt1

print len(sums.keys())
#print sums
exit(0)
a = sums.keys()
a.sort()
for i in range(0, len(a)) :
    for j in range(0, len(sums[a[i]])) :
        print (a[i], sums[a[i]][j][2], sums[a[i]][j][3])
