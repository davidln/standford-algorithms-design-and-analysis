def swap(l, s, e) :
    aux = l[s]
    l[s] = l[e]
    l[e] = aux
    
def choose_pivot1(l, s, e):
    return s

def choose_pivot2(l, s, e):
    return e

def choose_pivot3(l, s, e):
    ln = e - s + 1
    m = s
    if ln % 2 == 0 : m += -1
    m += int(ln/2)

    aux = [(l[s], s), (l[m], m), (l[e], e)]
    aux.sort()
    mean, mean_pos = aux[1]
    return mean_pos

def choose_pivot(l, s, e):
    global pivot_function
    return pivot_function(l, s, e)

def partition_p(l, p, s, e) :
    global comparisons
    comparisons = comparisons + e - s

    if p > s :
        swap(l, p, s)
        p = s

    if e - s == 1 :
        if l[e] < l[s] :
            swap(l, s, e)
            return e
        else :
            return s
    i = s + 1
    j = s + 1
    while j <= e :
        if l[j] < l[p] :
            swap(l, i, j)
            i+=1
        j += 1

    swap(l, p, i-1)
    # return the new pivot position
    return i-1

def qsort(l, s, e):
    global debug
    if e <= s :
        return None

    p = choose_pivot(l, s, e)
    if debug : print "antes", s, p, e, l[s: e+1]
    p = partition_p(l, p, s, e)
    if debug : print "despues", s, p, e, l[s: e+1]

    qsort(l, s, p-1) # order left part, from starting pos to pivot pos -1
    qsort(l, p+1, e) # order right part, from pivot pos +1 to end position

    return l

comparisons = 0
pivot_function = None
debug = False
def test(pf, d = False) :
    print(pf)

    global comparisons
    global pivot_function
    global debug

    comparisons = 0
    pivot_function = pf
    debug = d

    fh = open('QuickSort.txt')
    numbers = []
    for i in fh:
        numbers.append(int(i))

    fh.close()

    if debug : numbers = numbers[:10]
    numbers_sorted = numbers[:]
    numbers_sorted.sort()

    qsort(numbers, 0, len(numbers)-1)

    if numbers == numbers_sorted :
        print "Both lists match"
    else :
        print "The lists don't match"
        print numbers

    print comparisons

if __name__ == "__main__":
    test(choose_pivot1)
    test(choose_pivot2)
    test(choose_pivot3)
