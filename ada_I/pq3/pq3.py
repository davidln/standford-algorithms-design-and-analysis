
# coding: utf-8

## Standford - Algorithms: Design and analisys Part I

### Programming Question 3

# The file contains the adjacency list representation of a simple undirected graph. There are 200 vertices labeled 1 to 200. The first column in the file represents the vertex label, and the particular row (other entries except the first column) tells all the vertices that the vertex is adjacent to. So for example, the 6th row looks like : "6 155 56 52 120 ......". This just means that the vertex with label 6 is adjacent to (i.e., shares an edge with) the vertices with labels 155,56,52,120,......,etc
# 
# Your task is to code up and run the randomized contraction algorithm for the min cut problem and use it on the above graph to compute the min cut. (HINT: Note that you'll have to figure out an implementation of edge contractions. Initially, you might want to do this naively, creating a new graph from the old every time there's an edge contraction. But you should also think about more efficient implementations.) (WARNING: As per the video lectures, please make sure to run the algorithm many times with different random seeds, and remember the smallest cut that you ever find.)

# In[143]:

#this list will hold the edges of the graph
#every edge will be a list of three elements [lower node, higher node, (lower node, higher node)].
#The third element is a tuple with the original edge
#the elements lower node and higher node will be changed by the algorithm
edges = []

#building the list of edges
fh = open("kargerMinCut.txt")
for line in fh:
    data = line.split()
    edge_t = int(data[0]) #edge tail
    for i in range(1,len(data)) :
        edge_h = int(data[i]) #edge head
        #the edges are repeated. The edge (i,j) is the same edge (j,i). We put only one edge on the list
        if edge_t < edge_h :
            edges.append([edge_t, edge_h, (edge_t, edge_h)])
fh.close()

#list of nodes
#when we select an edge we'll fuse both nodes and left the lower one
#after the fusion we'll have to update edges replacing the name of the node fused
nodes = range(1, int(data[0])+1)


# In[110]:

#this function will replace node_old by node_new in the list of edges as a consequence of the fusion of the two nodes
def replace_node(edges, node_old, node_new) :
    for i in range(0, len(edges)) :
        edg = edges[i]
        if edg[0] == node_old :
            edges[i][0] = node_new
        if edg[1] == node_old :
            edges[i][1] = node_new


# In[111]:

#this function will look for loop edges and will eliminate it from the list of edges
def delete_loops(edges) :
    for i in range(len(edges)-1, -1, -1) :
        edg = edges[i]
        if edg[0] == edg[1] :
            edges.pop(i)


# In[112]:

import random
def min_cut(edges, nodes) :
    while len(nodes) > 2 :
        random_e = random.choice(edges) #randomly selecting an edge
        lower_n = random_e[0]
        higher_n = random_e[1]
        replace_node(edges, higher_n, lower_n) #joining both nodes of the edge
        delete_loops(edges)
        nodes.remove(higher_n) #deleting the joined node


# In[ ]:

import copy
import math

N = len(nodes)
num_executions = int(N*math.log(N)) + 1 # as showed in the lectures

num_cuts = []

while num_executions > 0 :
    edges1 = copy.deepcopy(edges)
    nodes1 = copy.deepcopy(nodes)
    min_cut(edges1, nodes1)
    num_cuts.append(len(edges1))
    num_executions -= 1

print min(num_cuts)


# In[ ]:



