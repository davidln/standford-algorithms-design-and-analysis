def msort(t, n=-1) :
    """
    Sorts the list t with a merge sort algorithm
    n is the length of the list t (optional). If not provided, the function computes the lenght itself
    returns the list t ordered. t is modified in the process
    """
    if n < 0: n = len(t)
    if n == 1: return t

    #recursive calls to sort the first and second half of t
    nf = n / 2
    ns = n - nf
    fh = msort(t[:nf], nf)
    sh = msort(t[nf:], ns)

    #now integrate both halves in the same list
    i = j = k = 0
    for i in range(0, n) :
        if k >= ns or (j<nf and fh[j] < sh[k]) :
            t[i] = fh[j]
            j += 1
            continue
        elif j>= nf or (k<ns and fh[j] > sh[k]) :
            t[i] = sh[k]
            k += 1
            continue
    return t

def count_inversions_brute_force(t) :
    """
    Count all the inversions (that is when index i<j and t[i]>t[j])  in the array t with brute force algorithm. O(n^2)
    """

    n = len(t)
    inv = 0
    for i in range(0, n) :
        for j in range(i, n) :
            if t[j] < t[i] :
                inv += 1
    return inv
    
def count_inversions(t, n=-1) :
    """
    Sorts the list t with a merge sort algorithm and count the number of inversions in the list t (that is when for index i<j t[i] > t[j]
    n is the length of the list t (optional). If not provided, the function computes the lenght itself
    returns a tupple (t, ninv) where the list t is ordered and the number of inversions. t is modified in the process
    """
    inv = 0
    if n < 0: n = len(t)
    if n == 1: return (inv,t)

    #recursive calls to sort the first and second half of t
    nf = n / 2
    ns = n - nf
    fi, fh = count_inversions(t[:nf], nf)
    si, sh = count_inversions(t[nf:], ns)
    inv = inv + fi + si

    #now integrate both halves in the same list
    i = j = k = 0
    for i in range(0, n) :
        if k >= ns or (j<nf and fh[j] < sh[k]) :
            t[i] = fh[j]
            j += 1
            continue
        elif j>= nf or (k<ns and fh[j] > sh[k]) :
            t[i] = sh[k]
            k += 1
            inv += nf - j
            continue
    return (inv, t)

def test_msort():
    a = range(0,10)
    b = a[:]
    b = msort(b)
    if (a != b) :
        print "test_msort: KO\n", a,b
        return
    print "test_msort: OK"

def test_count_inversions():
    a = [1, 3, 5, 2, 4, 6]
    res1 = (count_inversions_brute_force(a), msort(a[:]))
    res2 = count_inversions(a)
    if (res1 != res2):
        print "test_count_inversions KO\n", res1, res2
        return
    print "test_count_inversions: OK"

if __name__ == "__main__" :
    test_msort()
    test_count_inversions()
