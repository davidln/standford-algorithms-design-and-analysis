#!/usr/bin/python
import sys

def update_distances(Gd, nd, E) :
    for (d, h, t) in E :
        if t in Gd : continue
        new_d = nd[h] + d
        if new_d < nd[t] :
            nd[t] = new_d

def get_closest_node(Gd, G) :
    min_node = None
    min_dist = float("inf")
    for (n, d) in G.items() :
        if n in Gd : continue
        if d < min_dist :
            min_node = n
            min_dist = d
    return (min_node, min_dist)
            
# Dijkstrat algorithm for the shortest path
def dsp(G, s, n) :
    # initializing the distances with an infinite value 
    node_distances = dict()
    for i in range(1, n+1) :
        node_distances[i] = float("inf")

    # the distance from the start node to itself
    node_distances[s] = 0

    # Dijkstrat graph nodes
    # keys -> node id's
    # values -> distance from s
    Gd = dict()
    nGd = 0

    while nGd < n :
        # find the node with the smallest distance
        (node, distance) = get_closest_node(Gd, node_distances)

        # adding the closest node and its distance to the Dijkstra graph
        Gd[node] = distance
        nGd += 1

        # getting the edges starting from node
        edges = G.pop(node)
        update_distances(Gd, node_distances, edges)

    print G
        
    return Gd, node_distances

# the grapth is a list of edges of the form: (weight, head, tail)
if (len(sys.argv) != 3) :
    print "Error: Not enough parameters"
    print "Usage:\n\t", sys.argv[0] + " input_file starting_node"
    exit(-1) 

input_file = sys.argv[1]
start_node = int(sys.argv[2])

num_nodes = 0
G = dict()
fh = open(input_file)
for line in fh :
    edges = line.split()

    if len(edges) > 0 :
        head = int(edges[0])
        del edges[0]

    if head not in G :
        G[head] = []

    for e in edges:
        e = e.split(',')
        tail = int(e[0])
        distance = int(e[1])
        G[head].append( (distance, head, tail) )
        num_nodes = max( [num_nodes, head, tail] )
fh.close()

# all the distances
D,nd = dsp(G.copy(), start_node, num_nodes)

print D[7], D[37], D[59], D[82], D[99], D[115], D[133], D[165], D[188], D[197]
print nd[7], nd[37], nd[59], nd[82], nd[99], nd[115], nd[133], nd[165], nd[188], nd[197]



