#!/usr/bin/python

import sys

class City:
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)
    def get_x(self):
        return self.x
    def get_y(self):
        return self.y
    def get_coords(self):
        return (self.x, self.y)
        
    def distanceTo(self, city_b):
        from math import sqrt
        (xb, yb) = city_b.get_coords()
        return sqrt((xb - self.x)**2 + (yb - self.y)**2)

    def equals(self, city_b) :
        if id(self) == id(city_b) : return True
        if self.x != city_b.x : return False
        if self.y != city_b.y : return False
        return True

    def __str__(self) :
        return str(self.get_coords())
    def __repr__(self) :
        return str(self.get_coords())


class TSP :
    def __init__(self, coords) :
        self.cities = []
        for (x,y) in coords :
            self.cities.append(City(x, y))
        self.N = len(self.cities)
        self.M = dict()
        self.start = 0
        self.calls = 0
        self.distance = 0.0
        self.D = []
        for i in range(0, len(self.cities)) :
            d = []
            for j in range(0, len(self.cities)) :
                d.append(self.cities[i].distanceTo(self.cities[j]))
            self.D.append(d)

    def solve(self) :
        (self.distance, path) = self.tsp(range(1, self.N), self.start)
        res = []
        for i in path :
            res.append(self.cities[i])
        return (self.distance, res)

    def total_distance(self) :
        return self.distance
    def num_calls(self) :
        return self.calls
    
    def convert(self, lcities) :
        mapbits = [0] * len(self.cities)
        for i in lcities :
            mapbits[i] = 1
        r = 0
        for b in mapbits :
            r = (r << 1) | b
        return r

    def ref_to_city(self, i) :
        return self.cities[i]
    
    def tsp(self, subgroup, end) :
        key = (self.convert(subgroup + [end]), end)
        if key in self.M : return self.M[key]
        
        if len(subgroup) == 2 :
            self.calls += 2
            d1 = self.D[self.start][subgroup[0]] + self.D[subgroup[0]][subgroup[1]] + self.D[subgroup[1]][end]
            d2 = self.D[self.start][subgroup[1]] + self.D[subgroup[1]][subgroup[0]] + self.D[subgroup[0]][end]
            if (d1 < d2) :
                self.M[key] = (d1, [self.start, subgroup[0], subgroup[1], end])
            else :
                self.M[key] = (d2, [self.start, subgroup[1], subgroup[0], end])
            return self.M[key]

        self.calls += 1
        options = []
        for i in range(0, len(subgroup)) :
            subset = subgroup[:]
            next_city = subset[i]
            cn = self.ref_to_city(next_city)
            del subset[i]
            (d, path) = self.tsp(subset, next_city)
            path = list(path)
            d += self.D[next_city][end]
            path.append(end)
            options.append((d, path))

        self.M[key] = min(options)
        return self.M[key]


lines = []
fh = open(sys.argv[1])
for line in fh:
    lines.append(map(float, line.split()))
fh.close()

n_cities = int(lines.pop(0)[0])


pq5 = TSP(lines)
(d, path) = pq5.solve()
print d, int(d), "\n"
for i in path :
    print i.get_x(), i.get_y()
print "\ntotal calls:", pq5.num_calls()

