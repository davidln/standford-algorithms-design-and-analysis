#!/usr/bin/python

from QUnionFind import *


print "reading file"
fh = open("clustering_big.txt")

first_line = True
N = 0
nbits = 24

data = dict()
node_id = 1
for line in fh :
    ll = map(int, line.split())
    key = tuple(ll)
    if first_line :
        (N, nbits) = ll
        first_line = False
        uf = QUnionFind(N)
        continue
    if key in data :
        # if we find a duplicate, then union those tow nodes now
        uf.union(data[key], node_id)
    else :
        data[key] = node_id
    node_id += 1
fh.close()
print "file read"

def swap_bit(word, pos) :
    
    wl = list(word)
    if wl[pos] == 0 :
        wl[pos] = 1
    else :
        wl[pos] = 0
    return tuple(wl)

def make_options(item) :
    options = []

    for i in range(0, len(item)) :
        options.append( swap_bit(item, i) )

    for i in range(0, len(item)) :
        for j in range(i+1, len(item)) :
            options.append( swap_bit(swap_bit(item, i), j) )
    return options



for (v1, n1) in data.items() :
    for v2 in make_options(v1) :
        if v2 in data :
            n2 = data[v2]
            uf.union(n1, n2)

print "Groups left: ", uf.get_groups()
print uf.check_groups()
