#!/usr/bin/python

class QUnionFind:
    def __init__(self, N) :
        self.unions = []
        self.rank = []
        self.groups = N
        for i in range(0, N+1) :
            self.unions.append(i)
            self.rank.append(0)
            
    def find(self, n):
        if self.unions[n] == n:
            return n
        self.unions[n] = self.find(self.unions[n])
        return self.unions[n]

    def union(self, p, q):
        pp = self.find(p)
        pq = self.find(q)
        
        if (pp == pq) :
            return

        self.groups -= 1
        if (self.rank[pp] > self.rank[pq]) :
            self.unions[pq] = pp
            return
        if (self.rank[pq] > self.rank[pp]) :
            self.unions[pp] = pq
            return
        
        #if they have the same rank pp will remain a root node
        self.unions[pq] = pp
        self.rank[pp] += 1
        
    def connected(self, p, q):
        return self.find(p) == self.find(q)

    def get_groups(self) :
        return self.groups
    
    def check_groups(self) :
        gs = dict()
        for i in range(1, len(self.unions)) :
            gs[self.find(i)] = 1
        return len(gs.keys())

    def toString(self) :
        print range(0, len(self.unions))
        print self.unions
        print self.rank
        print self.groups

