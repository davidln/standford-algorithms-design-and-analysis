
# coding: utf-8

# In[1]:

from QUnionFind import *


# In[2]:

fh = open("clustering1.txt")
edges = []
N = 0
for line in fh:
    li = map(int, line.split())
    if len(li) == 1 :
        N = li[0]
        continue
    (n1, n2, c) = li
    edges.append((c, n1, n2))
fh.close()


# In[3]:

edges.sort()
uf = QUnionFind(N)


# In[4]:

def read_edges(edges, uf, i) :
    (c, n1, n2) = edges[i]
    uf.union(n1, n2)
    i += 1
    while c == edges[i][0] :
        uf.union(edges[i][1], edges[i][2])
        i += 1
    return i


# In[5]:

i = 0
while uf.get_groups() > 4 :
    i = read_edges(edges, uf, i)


# In[6]:

# the next edges may be part of the same cluster
# so we keep on reading edges until the nodes
# of that edge are from distinct clusters
for j in range(i, len(edges)) :
    (c, n1, n2) = edges[j]
    if uf.connected(n1, n2) == False:
        print (j, edges[j])
        break

