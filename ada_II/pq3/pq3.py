#!/usr/bin/python

# coding: utf-8

# In[19]:

import sys
import resource

sys.setrecursionlimit(3000)
resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY))


# In[20]:

fh = open(sys.argv[1])
lines = []
for line in fh :
    lines.append(line)
fh.close()


# In[21]:

lines = map(lambda(x) : map(int, x.split()), lines)


# In[22]:

#lines = [[6, 4], [3, 4], [2, 3], [4, 2], [4, 3]]


# In[23]:

W = lines[0][0]
N = lines[0][1]

if len(lines) != N + 1 : print error
    
M = {}


# In[24]:


def get_value(n) :
    VALUE  = 0
    return n[VALUE]
def get_weight(n) :
    WEIGHT = 1
    return n[WEIGHT]


# In[25]:

def knapsack(M, items, i, x) :
    if i == 0 : return 0
    wi = get_weight(items[i])
    vi = get_value(items[i])
    #if wi > x : return 0
    
    if (i, x) in M : return M[(i, x)]
    if x-wi < 0 :
        M[(i,x)] = knapsack(M, items, i-1, x)
    else :
        M[(i,x)] = max([knapsack(M, items, i-1, x), vi + knapsack(M, items, i-1, x-wi)])
    return M[(i,x)]


# In[26]:

print knapsack(M, lines, N, W)

