#!/usr/bin/python

import sys


def load_graph(filename) :
    fh = open(filename)
    lines = []
    for line in fh :
        lines.append(line)
    fh.close()
    
    lines = map(lambda(x) : map(int, x.split()), lines)
    
    N = lines[0][0]
    n_edges = lines[0][1]
    
    del lines[0]
    return (N, n_edges, lines)

def add_virtual_edges(N) :
    ve = []
    for i in range(1, N+1) :
        ve.append([0, i, 0])
    return ve


def build_head_tail_graph(E, N) :
    G = dict()
    for i in range(0, N+1) :
        G[i] = dict()

    for i in E:
        (t, h, c) = i
        G[h][t] = c
    return G

def build_tail_head_graph(E, N) :
    G = dict()
    for i in range(0, N+1) :
        G[i] = dict()

    for i in E:
        (t, h, c) = i
        G[t][h] = c
    return G


def get_min_cost_in_degree(L, in_edges):
    C = []
    for (f, c) in in_edges.items() :
        C.append(L[f] + c)
    if len(C) == 0 : return float("Inf")
    return min(C)

def bellman_ford(G, N, s) :
    Ln = []
    for i in range(0, N+1) :
        if i == s :
            Ln.append(0)
        else :
            Ln.append(float("Inf"))

    for i in range(1, N+1) :
        if s in G[i] :
            Ln[i] = G[i][s]

    Lp = Ln[:]
    
    all_the_same = True
    for i in range(2, N+1) :
        aux = Lp
        Lp = Ln
        Ln = aux
        all_the_same = True
        for v in range(1, N+1):
            cv = get_min_cost_in_degree(Lp, G[v])
            if Lp[v] > cv :
                Ln[v] = cv
                all_the_same = False
            else :
                Ln[v] = Lp[v]
        if all_the_same : break
    if all_the_same : return (Ln, Lp, False)
    
    negative_cycle = (Ln != Lp)
    return (Ln, Lp, negative_cycle)


print "Loading graph"
(N, ne, E) = load_graph(sys.argv[1])
print "Creating data structures"
G = build_head_tail_graph(E, N)

(s, e, c) = (0, 0, float("Inf"))
for i in range(1, N+1) :
    print "Starting from vertex:", i, (s, e, c)
    (L, Lp, ncf) = bellman_ford(G, N, i)
    if ncf :
        print("Has a negative cost cycle")
        break
    for l in range(0, len(L)) :
        if i == l : continue
        if L[l] < c :
            c = L[l]
            s = i
            e = l

print "Min cost: ", c, " from: ", s, " to: ", e
