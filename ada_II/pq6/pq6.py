#!/usr/bin/python

# 2-SAT problem solution using Kosaraju's algorithm for SCC
# see: http://en.wikipedia.org/wiki/2-SAT#Strongly_connected_components

import sys
import resource

sys.setrecursionlimit(1000000)
resource.setrlimit(resource.RLIMIT_STACK, (resource.RLIM_INFINITY, resource.RLIM_INFINITY))

def add_edge(g, h, t) :
    if h in g :
        g[h].append(t)
    else :
        g[h] = [t]

def is_satisfiable(scc) :
    for i in scc.keys() :
        if -i in scc :
            return False
    return True

if len(sys.argv) > 1:
    input_file = sys.argv[1]
else :
    input_file = "test_nsat.txt"
    
fh = open(input_file)

# number of variables
N = int(fh.readline())

# conditions
#C = map(lambda x: x.split(), fh.readlines())
C = map(lambda x: map(int, x), map(lambda x: x.split(), fh.readlines()))

fh.close()

D = dict() # edges in direct order
R = dict() # edges in reverse order

# for an expresion of the form c1 or c2 we add a couple of edges to the graph
# not c1 to c2
# not c2 to c1
for (c1, c2) in C :
    add_edge(D, -1*c1, c2)
    add_edge(D, -1*c2, c1)
    add_edge(R, c2, -1*c1)
    add_edge(R, c1, -1*c2)
    

def dfs_s1(ft, nodes, edges, s) :
    nodes[s] = 1
    if s in edges :
        for v in edges[s] :
            if v not in nodes :
                dfs_s1(ft, nodes, edges, v)
    ft.append(s)

def dfs_s2(scc, nodes, edges, s) :
    nodes[s] = 1
    if s in edges :
        for v in edges[s] :
            if v not in nodes :
                dfs_s2(scc, nodes, edges, v)
    scc[s] = 1

# first stage of the algorithm, calculating the finishing times
# in the reverse graph R
visited_nodes = dict()
nodes_by_finishing_time = []

for i in range(N, -N-1, -1) :
    if i == 0 : continue
    if i not in visited_nodes :
        dfs_s1(nodes_by_finishing_time, visited_nodes, R, i)

# second stage of the algorithm, finding the SCC
# iterating the nodes in reverse order of their finishing times
nodes_by_finishing_time.reverse()
visited_nodes = dict()

SCCs = []
for i in nodes_by_finishing_time :
    if i not in visited_nodes :
        scc = {}
        dfs_s2(scc, visited_nodes, D, i)
        sat = is_satisfiable(scc)
        if not sat :
            print "Not satisfiable"
            break
        SCCs.append(scc.keys())
if sat : print "Satisfiable"
#print SCCs
